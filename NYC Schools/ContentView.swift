//
//  ContentView.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/15/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ListOfSchoolsView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
