//
//  NYC_SchoolsApp.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/15/23.
//

import SwiftUI

@main
struct NYC_SchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
