//
//  School.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/15/23.
//

import Foundation

struct School: Codable {
    let name: String
    let databaseIdentifier: String
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case databaseIdentifier = "dbn"
    }
}
