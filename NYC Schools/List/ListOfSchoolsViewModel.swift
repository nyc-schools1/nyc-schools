//
//  ListOfSchoolsFetcher.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/15/23.
//

import Foundation

final class ListOfSchoolsViewModel: ObservableObject {
    @Published private(set) var schools = [School]()
    
    private let urlString: String
    
    init(urlString: String) {
        self.urlString = urlString
    }
    
    func fetchSchools() async throws {
        guard let url = URL(string: urlString) else {
            throw URLError(.badURL)
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        
        let newSchools = try JSONDecoder().decode([School].self, from: data)
        
        await MainActor.run {
            self.schools = newSchools
        }
    }
}
