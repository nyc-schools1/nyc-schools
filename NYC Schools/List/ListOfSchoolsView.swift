//
//  SwiftUIView.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/15/23.
//

import SwiftUI

struct ListOfSchoolsView: View {
    @ObservedObject private var viewModel = ListOfSchoolsViewModel(urlString: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
    @State private var failedToFetchSchools = false
    @State private var failedToFetchSchoolsAlertDetails: String?
    private let failedToFetchSchoolsAlertTitle = "Failed to fetch schools."
    
    var body: some View {
        List(viewModel.schools.indices, id: \.self) { index in
            NavigationLink {
                DetailView(schoolDatabaseIdentifier: viewModel.schools[index].databaseIdentifier)
                    .navigationTitle("SAT Performance")
            } label: {
                Text(viewModel.schools[index].name)
            }
        }
        .task {
            do {
                try await viewModel.fetchSchools()
            } catch {
                failedToFetchSchools = true
                failedToFetchSchoolsAlertDetails = error.localizedDescription
            }
        }
        .alert(failedToFetchSchoolsAlertTitle, isPresented: $failedToFetchSchools, presenting: failedToFetchSchoolsAlertDetails, actions: {_ in })
        { errorDetails in
            Text(errorDetails)
        }
        .navigationTitle("NYC Schools")
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        ListOfSchoolsView()
    }
}
