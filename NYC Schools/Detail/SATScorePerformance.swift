//
//  AverageSATScore.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/16/23.
//

import Foundation

struct SATScorePerformance: Codable {
    let numberOfSATTakers: String
    let criticalReadingAverage: String
    let mathAverage: String
    let writingAverage: String
    
    enum CodingKeys: String, CodingKey {
        case numberOfSATTakers = "num_of_sat_test_takers"
        case criticalReadingAverage = "sat_critical_reading_avg_score"
        case mathAverage = "sat_math_avg_score"
        case writingAverage = "sat_writing_avg_score"
    }
}
