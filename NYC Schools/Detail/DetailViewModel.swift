//
//  DetailViewModel.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/16/23.
//

import Foundation

final class DetailViewModel: ObservableObject {
    @Published var satScorePerformanceRows = [(title: String, value: String)]()
    
    private let baseURLString: String
    
    init(baseURLString: String) {
        self.baseURLString = baseURLString
    }
    
    func fetchSATScorePerformanceRows(schoolDatabaseIdentifier: String) async throws {
        guard let url = URL(string: baseURLString + "?dbn=\(schoolDatabaseIdentifier)") else {
            throw URLError(.badURL)
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        
        guard let satScorePerformance = try JSONDecoder().decode([SATScorePerformance].self, from: data).first else {
            throw NoSATPerformanceFound()
        }
        
        
        await MainActor.run {
            self.satScorePerformanceRows = [
                (title: "Number of Test Takers", value: satScorePerformance.numberOfSATTakers),
                (title: "Critial Reading Average", value: satScorePerformance.criticalReadingAverage),
                (title: "Math Average", value: satScorePerformance.mathAverage),
                (title: "Writing Average", value: satScorePerformance.writingAverage),
            ]
        }
    }
}

struct NoSATPerformanceFound: LocalizedError {
    var errorDescription: String? {
        return "No SAT Performance Found."
    }
}

