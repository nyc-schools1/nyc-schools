//
//  DewtailView.swift
//  NYC Schools
//
//  Created by Rafael Rincon on 8/16/23.
//

import SwiftUI

struct DetailView: View {
    @ObservedObject private var viewModel = DetailViewModel(baseURLString: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
    @State private var failedToFetchSATScorePerformance = false
    @State private var failedToFetchSATScorePerformanceAlertDetails: String?
    
    let schoolDatabaseIdentifier: String
    private let failedToFetchAverageSATScoresAlertTitle = "Failed to fetch schools."
    
    var body: some View {
        List(viewModel.satScorePerformanceRows.indices, id: \.self) { index in
            HStack {
                Text(viewModel.satScorePerformanceRows[index].title)
                Spacer()
                Text(viewModel.satScorePerformanceRows[index].value)
            }
        }
        .task {
            do {
                try await viewModel.fetchSATScorePerformanceRows(schoolDatabaseIdentifier: schoolDatabaseIdentifier)
            } catch {
                failedToFetchSATScorePerformance = true
                failedToFetchSATScorePerformanceAlertDetails = error.localizedDescription
            }
        }
        .alert(failedToFetchAverageSATScoresAlertTitle, isPresented: $failedToFetchSATScorePerformance, presenting: failedToFetchSATScorePerformanceAlertDetails, actions: {_ in })
        { errorDetails in
            Text(errorDetails)
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(schoolDatabaseIdentifier: "")
    }
}
