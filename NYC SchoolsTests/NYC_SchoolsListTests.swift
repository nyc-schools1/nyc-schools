//
//  NYC_SchoolsTests.swift
//  NYC SchoolsTests
//
//  Created by Rafael Rincon on 8/15/23.
//

import XCTest
@testable import NYC_Schools

final class NYC_SchoolsListTests: XCTestCase {
    /** Given the url passed to the view model is bad, when the fetch schools method is called, then an error should be thrown.*/
    func testURLIsBad_fetchSchools_errorIsThrown() async {
        let viewModel = ListOfSchoolsViewModel(urlString: "exampleBadURL")
        
        do {
            try await viewModel.fetchSchools()
            
            XCTFail("No error was thrown")
        } catch {}
    }
    
    /** Given the url passed to the view model is good, when the fetch schools method is called, then the list of schools should be non-empty.*/
    func testURLIsGood_fetchSchools_listOfSchoolsIsNonEmpty() async throws {
        let viewModel = ListOfSchoolsViewModel(urlString: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        
        try await viewModel.fetchSchools()
        
        XCTAssertFalse(viewModel.schools.isEmpty)
    }
}
