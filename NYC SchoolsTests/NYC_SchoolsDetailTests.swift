//
//  NYC_SchoolsTests.swift
//  NYC SchoolsTests
//
//  Created by Rafael Rincon on 8/15/23.
//

import XCTest
@testable import NYC_Schools

final class NYC_SchoolsDetailTests: XCTestCase {
    let goodSchoolDatabaseIdentifier = "01M292"
    let goodBaseURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    /** Given the url passed to the view model is bad and the school database identifier is good, when the fetch schools method is called, then an error should be thrown.*/
    func testURLIsBadAndSchoolDatabaseIdentifierIsGood_fetchSchools_errorIsThrown() async {
        let viewModel = DetailViewModel(baseURLString: "exampleBadURL")
        
        do {
            try await viewModel.fetchSATScorePerformanceRows(schoolDatabaseIdentifier: goodSchoolDatabaseIdentifier)
            
            XCTFail("No error was thrown")
        } catch {}
    }
    
    /** Given the url passed to the view model is good and the school database identifier is bad, when the fetch schools method is called, then an error should be thrown.*/
    func testURLIsGoodAndSchoolDatabaseIdentifierIsBad_fetchSchools_errorIsThrown() async {
        let viewModel = DetailViewModel(baseURLString: "exampleBadURL")
        
        do {
            try await viewModel.fetchSATScorePerformanceRows(schoolDatabaseIdentifier: "exampleBadSchoolDatabaseIdentifier")
            
            XCTFail("No error was thrown")
        } catch {}
    }
    
    /** Given the url passed to the view model is good, when the fetch schools method is called, then the list of schools should be non-empty.*/
    func testURLIsGoodAndSchoolDatabaseIdentifierIsGood_fetchSchools_satPerformanceRowsIsNonEmpty() async throws {
        let viewModel = DetailViewModel(baseURLString: goodBaseURL)
        
        try await viewModel.fetchSATScorePerformanceRows(schoolDatabaseIdentifier: goodSchoolDatabaseIdentifier)
        
        XCTAssertFalse(viewModel.satScorePerformanceRows.isEmpty)
    }
}
